import flask
from flask import jsonify, request
from werkzeug.exceptions import HTTPException
import urllib.request, json
from services.shoePrice.ShoePriceService import ShoePriceService
from constants.ShoePrice import ShoePrice

app = flask.Flask(__name__)
app.config["DEBUG"] = True


@app.route('/api/shoe-price/<int(min=1, max=6):id>', methods=['GET'])
def fetch_shoe_price(id):
    # id = int(request.view_args['id'])
    # url = f"http://localhost:9090/fetch-random-price/{id}"
    # response = urllib.request.urlopen(url)
    # data = response.read()
    # return data
    shoePriceService = ShoePriceService()
    original_price = shoePriceService.get_random_shoe_price()
    return jsonify({
            ShoePrice.ORIGINAL_PRICE_KEY: original_price,
            ShoePrice.DISCOUNT_PRICE_KEY: shoePriceService.get_discounted_price(original_price)
        })

@app.errorhandler(HTTPException)
def handle_exception(e):
    """Return generic JSON HTTP errors"""
    response = e.get_response()
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })
    response.content_type = "application/json"
    return response

if __name__ == '__main__': 
    app.run(host="localhost", port=8081, debug=True)
