import random

from constants.ShoePrice import ShoePrice

class ShoePriceService():

    def __init__(self):
        self.min_price = ShoePrice.MIN_PRICE
        self.max_price = ShoePrice.MAX_PRICE
        self.discount = ShoePrice.DISCOUNT_RATE
        self.decimal = ShoePrice.PRICE_DECIMAL

    def get_random_shoe_price(self):
        """
        Get a random price
        """
        return self.generate_random_price(self.min_price, self.max_price, self.decimal)

    def generate_random_price(self, min, max, decimal):
        """
        Generate a random price based off of passed in min/max prices and number of decimals
        :param min
        :param max
        :param decimal
        """
        return round(random.uniform(min, max-1) + random.random(), decimal)
    
    def get_discounted_price(self, original_price):
        """
        Get discount price for the given original price
        :param original_price
        """
        return round(original_price*self.discount, self.decimal)