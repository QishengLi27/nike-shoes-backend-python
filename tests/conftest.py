import pytest
from api import app
from services.shoePrice.ShoePriceService import ShoePriceService

@pytest.fixture
def client():
  client = app.test_client()
  return client

@pytest.fixture(scope="module")
def shoePriceService():
  return ShoePriceService()