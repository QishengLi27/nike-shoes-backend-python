from unittest.mock import MagicMock
from constants.ShoePrice import ShoePrice
from services.shoePrice.ShoePriceService import ShoePriceService
    
def test_get_random_shoe_price_returns_price():
    """
    Test get_random_shoe_price logic
    """
    expect_result = 12.99
    test_service = ShoePriceService()

    # Mock generate_random_price call inside the method
    test_service.generate_random_price = MagicMock(name='method', return_value=expect_result)

    assert test_service.get_random_shoe_price() == expect_result
    # Check if generate_random_price is called with correct params
    test_service.generate_random_price.assert_called_once_with(ShoePrice.MIN_PRICE, ShoePrice.MAX_PRICE, ShoePrice.PRICE_DECIMAL)

def test_generate_random_price_returns_price_correct_method_calls(mocker, shoePriceService):
    """
    Test generate_random_price logic
    """
    # Mock required built in methods
    mock_round = mocker.patch('builtins.round')
    mock_random_uniform = mocker.patch('random.uniform')
    mock_random_random = mocker.patch('random.random')

    original_price = 10
    expect_result = original_price*ShoePrice.DISCOUNT_RATE
    mock_random_uniform.return_value = 1
    mock_random_random.return_value = 1
    mock_round.return_value = expect_result

    result = shoePriceService.generate_random_price(ShoePrice.MIN_PRICE, ShoePrice.MAX_PRICE, ShoePrice.PRICE_DECIMAL)
    assert result == expect_result
    # Check if methods are called with correct params
    mock_round.assert_called_once_with(2, 2)
    mock_random_uniform.assert_called_once_with(ShoePrice.MIN_PRICE, ShoePrice.MAX_PRICE-1)
    mock_random_random.assert_called_once()

def test_generate_random_price_returns_price_within_price_range(shoePriceService):
    """
    Test generate_random_price:
    value returned from the method is within the given price range
    """
    result = shoePriceService.generate_random_price(ShoePrice.MIN_PRICE, ShoePrice.MAX_PRICE, ShoePrice.PRICE_DECIMAL)
    assert result > ShoePrice.MIN_PRICE and result < ShoePrice.MAX_PRICE

def test_generate_random_price_returns_random_price_each_time(shoePriceService):
    """
    Test generate_random_price:
    value returned from the method should be random
    """
    result_first = shoePriceService.generate_random_price(ShoePrice.MIN_PRICE, ShoePrice.MAX_PRICE, ShoePrice.PRICE_DECIMAL)
    result_second = shoePriceService.generate_random_price(ShoePrice.MIN_PRICE, ShoePrice.MAX_PRICE, ShoePrice.PRICE_DECIMAL)
    assert result_first is not result_second

def test_get_discounted_price_returns_correct_method_calls(mocker, shoePriceService):
    """
    Test get_discounted_price logic
    """
    # Mock required built in method
    mock_round = mocker.patch('builtins.round')

    original_price = 10
    expect_result = original_price*ShoePrice.DISCOUNT_RATE
    mock_round.return_value = expect_result

    assert shoePriceService.get_discounted_price(original_price) == expect_result
    # Check if the round() method are called with correct params
    mock_round.assert_called_once_with(expect_result, 2)