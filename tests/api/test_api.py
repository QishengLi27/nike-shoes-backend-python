import json
from constants.ShoePrice import ShoePrice

def test_fetch_shoe_price_with_correct_id_has_correct_status_code(client):
    """
    Test fetch_shoe_price_with_id:
    Return correct status code if correct id is provided
    """
    id = 1
    url = f'/api/shoe-price/{id}'
    response = client.get(url)
    assert response.status_code == 200

def test_fetch_shoe_price_with_no_id_has_correct_status_code(client):
    """
    Test fetch_shoe_price_with_id:
    Return correct status code if id is not provided
    """
    response = client.get('/api/shoe-price/')
    assert response.status_code == 404

def test_fetch_shoe_price_with_wrong_id_has_correct_status_code(client):
    """
    Test fetch_shoe_price_with_id:
    Return correct status code if the given id is not allowed
    """
    id_char = 'a'
    url = f'/api/shoe-price/{id_char}'
    response = client.get(url)
    assert response.status_code == 404

    id_out_of_range = 7
    url = f'/api/shoe-price/{id_out_of_range}'
    response = client.get(url)
    assert response.status_code == 404

def test_fetch_shoe_price_with_id_has_correct_response(client):
    """
    Test fetch_shoe_price_with_id:
    Return correct response if id is provided
    """
    id = 1
    url = f'/api/shoe-price/{id}'
    response = client.get(url)
    result = json.loads(response.data.decode())
    assert ShoePrice.ORIGINAL_PRICE_KEY in result and ShoePrice.DISCOUNT_PRICE_KEY in result
    assert type(result[ShoePrice.ORIGINAL_PRICE_KEY]) is float and type(result[ShoePrice.DISCOUNT_PRICE_KEY]) is float
