## Nike Interview Backend Code Challenge [Python]
#

### Run the project
To run the project, make sure to have following packages installed on your machine:
`python`, `pip`, `flask`, and `virtualenv`.
And then follow the steps below:
1. virtualenv venv
2. source venv/bin/activate
3. pip install -r requirements.txt
4. python api.py

And you can do the get request to http://localhost:8081/api/shoe-price/1

```
Response:
{
    {
        "discountPrice": 116.98, 
        "originalPrice": 194.96
    }
}
```
#
### Run the tests
After required packages are installed, you can run api/unit tests with `pytest`.
For example:
`pytest -v`
#
### What I've done
1. Updated project initial setup to run with virtual env and use requirements.txt to manage packages.
2. Modified the original API and added logic to return random original price and discount price.
3. Added a generic HTTP exception handler.
4. Restructured code to use constants and services.
5. Added API tests and service level unit tests using pytest.
6. Added gitlab CI to run tests on merge requests.

#
### If I have more time, I would:
1. Add database support to store/get/update shoe’s price data.
2. Add build process to CICD and host the project on a live site.
3. Add API token validation
4. Gather more requirements details

#
### Doubts & Assumptions
1. Requirements for API is not clear. Should we just return a random price or is there any required logic that needs to be added to the fetch price API (put min/max price range into the consideration?)?

#

### Other notes
Was a fun and creative test lol. First time doing a coding test that asks to add CI pipeline and tests.
