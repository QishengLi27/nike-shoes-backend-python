
class ShoePrice(object):

    MIN_PRICE: int = 0

    MAX_PRICE: int = 300

    DISCOUNT_RATE: float = .6

    PRICE_DECIMAL: int = 2

    ORIGINAL_PRICE_KEY: str = 'originalPrice'

    DISCOUNT_PRICE_KEY: str = 'discountPrice'
